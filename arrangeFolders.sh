mainDir=$1
#list sub directories
dirs=($(find $mainDir -mindepth 1 -maxdepth 1 -type d))
for dir in "${dirs[@]}"; do
    echo $dir
    mkdir $dir/xml
    mv $dir/*.xml $dir/xml
done